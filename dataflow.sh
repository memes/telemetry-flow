#!/bin/sh
# Deploy pipeline as a Dataflow templated pipeline
#
# Not prod safe, but working.

PROJECT=telemetry-demo-5677f116
BUCKET=telemetry-demo-63166d7e
DATASET=telemetry

# Cancel any running jobs
gcloud dataflow jobs list --project ${PROJECT} \
    --filter='STATE:Running' \
    --format='value(JOB_ID)' | xargs gcloud dataflow jobs cancel --project ${PROJECT}

# Update template metadata from local file
gsutil cp ./config/telemetry_metadata gs://${BUCKET}/dataflow/templates/

# Build and deploy as template
./gradlew build && \
    java -jar ./telemetry/build/libs/telemetry-1.1-all.jar \
        --streaming \
        --runner=DataflowRunner \
        --project=${PROJECT} \
        --stagingLocation=gs://${BUCKET}/dataflow/stage \
        --tempLocation=gs://${BUCKET}/dataflow/tmp \
        --templateLocation=gs://${BUCKET}/dataflow/templates/telemetry
[ $? -ne 0 ] && exit

# Launch a new job to execute this pipeline
gcloud beta dataflow jobs run iot-telemetry-$(date +%s) \
    --project=${PROJECT} \
    --gcs-location=gs://${BUCKET}/dataflow/templates/telemetry \
    --parameters subscription=projects/${PROJECT}/subscriptions/streaming-sub,\
targetProject=${PROJECT},\
targetDataset=${DATASET}