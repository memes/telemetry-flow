#!/bin/sh
# Direct runner on laptop

PROJECT=telemetry-demo-5677f116
BUCKET=telemetry-demo-63166d7e

./gradlew build && \
    java -jar ./telemetry/build/libs/telemetry-1.1-all.jar \
        --streaming \
        --runner=DirectRunner \
        --project=${PROJECT} \
        --targetProject=${PROJECT} \
        --subscription=projects/${PROJECT}/subscriptions/streaming-sub \
        --tempLocation=gs://${BUCKET}/dataflow/tmp