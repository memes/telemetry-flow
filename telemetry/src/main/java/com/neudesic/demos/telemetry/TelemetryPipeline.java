package com.neudesic.demos.telemetry;

import com.neudesic.demos.telemetry.adapters.bigquery.ArchiveToTableRow;
import com.neudesic.demos.telemetry.adapters.bigquery.TelemetryToTableRow;
import com.neudesic.demos.telemetry.adapters.pubsub.PubSubToArchive;
import com.neudesic.demos.telemetry.adapters.pubsub.PubSubToTelemetry;
import com.neudesic.demos.telemetry.models.Archive;
import com.neudesic.demos.telemetry.models.Telemetry;

import org.apache.beam.runners.dataflow.options.DataflowPipelineOptions;
import org.apache.beam.sdk.Pipeline;
import org.apache.beam.sdk.io.gcp.bigquery.BigQueryIO;
import org.apache.beam.sdk.io.gcp.pubsub.PubsubIO;
import org.apache.beam.sdk.io.gcp.pubsub.PubsubMessage;
import org.apache.beam.sdk.options.Default;
import org.apache.beam.sdk.options.Description;
import org.apache.beam.sdk.options.PipelineOptionsFactory;
import org.apache.beam.sdk.options.Validation;
import org.apache.beam.sdk.options.ValueProvider;
import org.apache.beam.sdk.transforms.ParDo;
import org.apache.beam.sdk.transforms.windowing.FixedWindows;
import org.apache.beam.sdk.transforms.windowing.GlobalWindows;
import org.apache.beam.sdk.transforms.windowing.TimestampCombiner;
import org.apache.beam.sdk.transforms.windowing.Window;
import org.apache.beam.sdk.values.PCollection;
import org.joda.time.Duration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Telemetry demo pipeline
 *
 * Incoming messages from IoT core are replicated as-is to `archive` table,
 * with streaming processing applied to determine OS and browser before
 * inserting to `telemetry` table.
 *
 */
public class TelemetryPipeline {
    private static final Logger LOG = LoggerFactory.getLogger(TelemetryPipeline.class);

    interface TelemetryPipelineOptions extends DataflowPipelineOptions {
        @Description("Name of the Pub/Sub subscription to use")
        @Validation.Required
        ValueProvider<String> getSubscription();
        void setSubscription(ValueProvider<String> subscription);

        @Description("Name of the project containing BQ dataset")
        @Validation.Required
        ValueProvider<String> getTargetProject();
        void setTargetProject(ValueProvider<String> targetProject);

        @Description("Name of the dataset to use in BQ")
        @Validation.Required
        ValueProvider<String> getTargetDataset();
        void setTargetDataset(ValueProvider<String> targetDataset);

        @Description("Name of the archive table")
        @Default.String("archive")
        ValueProvider<String> getArchiveTable();
        void setArchiveTable(ValueProvider<String> archiveTable);

        @Description("Description for archive table, optional")
        @Default.String("Archived Pub/Sub messages from IoT")
        ValueProvider<String> getArchiveTableDescription();
        void setArchiveTableDescription(ValueProvider<String> archiveTableDescription);

        @Description("Name of the telemetry table")
        @Default.String("telemetry")
        ValueProvider<String> getTelemetryTable();
        void setTelemetryTable(ValueProvider<String> telemetryTable);

        @Description("Description for telemetry table, optional")
        @Default.String("Telemetry data from IoT")
        ValueProvider<String> getTelemetryTableDescription();
        void setTelemetryTableDescription(ValueProvider<String> telemetryTableDescription);
    }

    public static void main(String[] args) {
        LOG.debug("Starting: args = {}", String.join(" ", args));
        PipelineOptionsFactory.register(TelemetryPipelineOptions.class);
        TelemetryPipelineOptions options = PipelineOptionsFactory
                .fromArgs(args)
                .withValidation()
                .as(TelemetryPipelineOptions.class);

        Pipeline pipeline = Pipeline.create(options);

        // Subscribe to a telemetry Pub/Sub topic
        PCollection<PubsubMessage> telemetry = pipeline
                .apply("Subscribing to telemetry messages",
                        PubsubIO.readMessagesWithAttributes()
                            .fromSubscription(options.getSubscription()))
                .apply("Set timestamp combiner to EARLIEST for global window",
                        Window.<PubsubMessage>into(new GlobalWindows())
                                .withTimestampCombiner(TimestampCombiner.EARLIEST));

        // Write a copy of the incoming messages as-is to the archive table
        telemetry.apply("Converting to message archive", ParDo.of(new PubSubToArchive()))
                .apply("Windowing by process timestamp",
                        Window.<Archive>into(FixedWindows.of(Duration.standardMinutes(1))))
                .apply("Writing archive messages to BigQuery",
                        BigQueryIO.<Archive>write()
                                .to(new BigQueryDestination<Archive>(options.getTargetProject(),
                                        options.getTargetDataset(),
                                        options.getArchiveTable(),
                                        options.getArchiveTableDescription()))
                                .withSchema(ArchiveToTableRow.schema())
                                .withFormatFunction(ArchiveToTableRow::tableRow)
                                .withWriteDisposition(BigQueryIO.Write.WriteDisposition.WRITE_APPEND)
                                .withCreateDisposition(BigQueryIO.Write.CreateDisposition.CREATE_IF_NEEDED));

        // This is the real stream
        telemetry.apply("Converting to telemetry objects", ParDo.of(new PubSubToTelemetry()))
                //.apply("Setting window timestamp from record",
                //        WithTimestamps.of(Telemetry::ts))
                .apply("Applying to one-minute windows",
                        Window.<Telemetry>into(FixedWindows.of(Duration.standardMinutes(1))))
                .apply("Writing telemetry records to BigQuery",
                        BigQueryIO.<Telemetry>write()
                .to(new BigQueryDestination<Telemetry>(options.getTargetProject(),
                        options.getTargetDataset(),
                        options.getTelemetryTable(),
                        options.getTelemetryTableDescription()))
                .withSchema(TelemetryToTableRow.schema())
                .withFormatFunction(TelemetryToTableRow::tableRow)
                        .withWriteDisposition(BigQueryIO.Write.WriteDisposition.WRITE_APPEND)
                        .withCreateDisposition(BigQueryIO.Write.CreateDisposition.CREATE_IF_NEEDED));
        pipeline.run();
    }
}
