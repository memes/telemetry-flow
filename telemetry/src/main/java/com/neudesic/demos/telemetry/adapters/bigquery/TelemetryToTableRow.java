package com.neudesic.demos.telemetry.adapters.bigquery;

import com.google.api.services.bigquery.model.TableFieldSchema;
import com.google.api.services.bigquery.model.TableRow;
import com.google.api.services.bigquery.model.TableSchema;
import com.neudesic.demos.telemetry.adapters.TimestampConversions;
import com.neudesic.demos.telemetry.models.Acceleration;
import com.neudesic.demos.telemetry.models.Position;
import com.neudesic.demos.telemetry.models.Telemetry;

import org.apache.beam.sdk.io.gcp.pubsub.PubsubMessage;
import org.apache.beam.sdk.transforms.DoFn;
import org.joda.time.Instant;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.Map;

/**
 * An adapter that can create a TableRow from a Telemetry instance.
 */
public class TelemetryToTableRow extends DoFn<PubsubMessage, Telemetry> {
    private static final Logger LOG = LoggerFactory.getLogger(TelemetryToTableRow.class);
    private static final String TS_FIELD = "ts";
    private static final String DEVICE_NUMBER_FIELD = "device_number_id";
    private static final String DEVICE_ID_FIELD = "device_id";
    private static final String DEVICE_REGISTRY_LOCATION_FIELD = "device_registry_location";
    private static final String DEVICE_REGISTRY_ID_FIELD = "device_registry_id";
    private static final String DEVICE_REGISTRY_PROJECT_ID_FIELD = "device_registry_project_id";
    private static final String SUBFOLDER_FIELD = "subfolder";
    private static final String PROCESS_TS_FIELD = "process_ts";
    private static final String UA_FIELD = "ua";
    private static final String LONGITUDE_FIELD = "longitude";
    private static final String LATITUDE_FIELD = "latitude";
    private static final String ACCELERATION_X_FIELD = "acceleration_x";
    private static final String ACCELERATION_Y_FIELD = "acceleration_y";
    private static final String ACCELERATION_Z_FIELD = "acceleration_z";
    private static final String ACCELERATION_MAGNITUDE_FIELD = "acceleration_magnitude";
    private static final String ERRORS_FIELD = "errors";
    private static final String BROWSER_FIELD = "browser";
    private static final String OS_FIELD = "os";
    private static final String GEOHASH_8 = "geohash_8";
    private static final TableSchema SCHEMA =
            new TableSchema().setFields(Arrays.asList(
                    new TableFieldSchema()
                            .setDescription("Timestamp for record, as set by originating device")
                            .setName(TS_FIELD)
                            .setType("TIMESTAMP")
                            .setMode("REQUIRED"),
                    new TableFieldSchema()
                            .setDescription("IoT Core unique device number")
                            .setName(DEVICE_NUMBER_FIELD)
                            .setType("STRING")
                            .setMode("REQUIRED"),
                    new TableFieldSchema()
                            .setDescription("IoT Core device id, as set by code")
                            .setName(DEVICE_ID_FIELD)
                            .setType("STRING")
                            .setMode("REQUIRED"),
                    new TableFieldSchema()
                            .setDescription("IoT Core device registry location")
                            .setName(DEVICE_REGISTRY_LOCATION_FIELD)
                            .setType("STRING")
                            .setMode("REQUIRED"),
                    new TableFieldSchema()
                            .setDescription("IoT Core device registry id")
                            .setName(DEVICE_REGISTRY_ID_FIELD)
                            .setType("STRING")
                            .setMode("REQUIRED"),
                    new TableFieldSchema()
                            .setDescription("IoT Core device registry project id")
                            .setName(DEVICE_REGISTRY_PROJECT_ID_FIELD)
                            .setType("STRING")
                            .setMode("REQUIRED"),
                    new TableFieldSchema()
                            .setDescription("Subfolder set by device")
                            .setName(SUBFOLDER_FIELD)
                            .setType("STRING")
                            .setMode("NULLABLE"),
                    new TableFieldSchema()
                            .setDescription("Timestamp of processing, as set by Dataflow")
                            .setName(PROCESS_TS_FIELD)
                            .setType("TIMESTAMP")
                            .setMode("REQUIRED"),
                    new TableFieldSchema()
                            .setDescription("User-agent string from device, as-is")
                            .setName(UA_FIELD)
                            .setType("STRING")
                            .setMode("NULLABLE"),
                    new TableFieldSchema()
                            .setDescription(
                                    "Longitude of device when record was created, may be null")
                            .setName(LONGITUDE_FIELD)
                            .setType("FLOAT64")
                            .setMode("NULLABLE"),
                    new TableFieldSchema()
                            .setDescription(
                                    "Latitude of device when record was created, may be null")
                            .setName(LATITUDE_FIELD)
                            .setType("FLOAT64")
                            .setMode("NULLABLE"),
                    new TableFieldSchema()
                            .setDescription(
                                    "Acceleration along x-axis, as set by the device, may be null")
                            .setName(ACCELERATION_X_FIELD)
                            .setType("FLOAT64")
                            .setMode("NULLABLE"),
                    new TableFieldSchema()
                            .setDescription(
                                    "Acceleration along y-axis, as set by the device, may be null")
                            .setName(ACCELERATION_Y_FIELD)
                            .setType("FLOAT64")
                            .setMode("NULLABLE"),
                    new TableFieldSchema()
                            .setDescription(
                                    "Acceleration along z-axis, as set by the device, may be null")
                            .setName(ACCELERATION_Z_FIELD)
                            .setType("FLOAT64")
                            .setMode("NULLABLE"),
                    new TableFieldSchema()
                            .setDescription("Calculated acceleration magnitude")
                            .setName(ACCELERATION_MAGNITUDE_FIELD)
                            .setType("FLOAT64")
                            .setMode("REQUIRED"),
                    new TableFieldSchema()
                            .setDescription("Errors during processing")
                            .setName(ERRORS_FIELD)
                            .setType("STRING")
                            .setMode("NULLABLE"),
                    new TableFieldSchema()
                            .setDescription("Browser type, as extracted from user-agent. Will be one of a small set of recognised implementations or 'unknown'")
                            .setName(BROWSER_FIELD)
                            .setType("STRING")
                            .setMode("REQUIRED"),
                    new TableFieldSchema()
                            .setDescription("Operating System, as extracted from user-agent. Will be one of a small set of recognised implementations or 'unknown'")
                            .setName(OS_FIELD)
                            .setType("STRING")
                            .setMode("REQUIRED"),
                    new TableFieldSchema()
                            .setDescription("8-character Geohash bounding box of location, equivalent to approximately 1.5 basketball courts. May be null")
                            .setName(GEOHASH_8)
                            .setType("STRING")
                            .setMode("NULLABLE")
            ));

    public static TableSchema schema() {
        LOG.debug("returning schema {}", SCHEMA);
        return SCHEMA;
    }

    public static TableRow tableRow(Telemetry telemetry) {
        LOG.debug("Converting to table row for {}", telemetry);
        if (telemetry == null) {
            LOG.warn("Telemetry object is null, returning null");
            return null;
        }
        TableRow row = new TableRow();
        try {
            row.set(TS_FIELD, TimestampConversions.toBigQueryTimestamp(telemetry.ts()));
            row = buildAttributes(row, telemetry.attributes());
            row.set(PROCESS_TS_FIELD, TimestampConversions.toBigQueryTimestamp(Instant.now()));
            row.set(UA_FIELD, telemetry.ua());
            row.set(BROWSER_FIELD, telemetry.browser() == null ? "unknown" : telemetry.browser());
            row.set(OS_FIELD, telemetry.os() == null ? "unknown" : telemetry.os());
            row = buildPosition(row, telemetry.position());
            row = buildAcceleration(row, telemetry.acceleration());
        } catch (Throwable t) {
            row.set(ERRORS_FIELD, t.toString());
        }
        return row;
    }

    private static String attributeToField(String name) {
        String field = null;
        switch (name) {
            case "deviceNumId":
                field = DEVICE_NUMBER_FIELD;
                break;

            case "deviceId":
                field = DEVICE_ID_FIELD;
                break;

            case "deviceRegistryLocation":
                field = DEVICE_REGISTRY_LOCATION_FIELD;
                break;

            case "deviceRegistryId":
                field = DEVICE_REGISTRY_ID_FIELD;
                break;

            case "projectId":
                field = DEVICE_REGISTRY_PROJECT_ID_FIELD;
                break;

            case "subFolder":
                field = SUBFOLDER_FIELD;
                break;
        }
        return field;
    }

    private static TableRow buildAttributes(TableRow row, Map<String, String> attributes) {
        LOG.debug("building attributes string from {}", attributes);
        if (attributes != null && !attributes.isEmpty()) {
            attributes.forEach((k, v) -> {
                String field = attributeToField(k);
                if (field != null && v != null) {
                    row.set(field, v);
                }
            });
        }
        return row;
    }

    private static TableRow buildPosition(TableRow row, Position position) {
        LOG.debug("building position fields from {}", position);
        String geohash8 = null;
        if (position != null) {
            if (position.longitude() != null) {
                row.set(LONGITUDE_FIELD, position.longitude());
            }
            if (position.latitude() != null) {
                row.set(LATITUDE_FIELD, position.latitude());
            }
            geohash8 = position.geohash8();
        }
        if (geohash8 != null) {
            row.set(GEOHASH_8, geohash8);
        }
        return row;
    }

    private static TableRow buildAcceleration(TableRow row, Acceleration acceleration) {
        LOG.debug("building acceleration fields from {}", acceleration);
        double magnitude = 0.0;
        if (acceleration != null) {
            if (acceleration.x() != null) {
                row.set(ACCELERATION_X_FIELD, acceleration.x());
            }
            if (acceleration.y() != null) {
                row.set(ACCELERATION_Y_FIELD, acceleration.y());
            }
            if (acceleration.z() != null) {
                row.set(ACCELERATION_Z_FIELD, acceleration.z());
            }
            magnitude = acceleration.magnitude();
        }
        row.set(ACCELERATION_MAGNITUDE_FIELD, magnitude);
        return row;
    }
}
