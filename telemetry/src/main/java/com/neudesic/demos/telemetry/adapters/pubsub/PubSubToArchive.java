package com.neudesic.demos.telemetry.adapters.pubsub;

import com.neudesic.demos.telemetry.models.Archive;

import org.apache.beam.sdk.io.gcp.pubsub.PubsubMessage;
import org.apache.beam.sdk.transforms.DoFn;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * An adapter that can create an Archive instance from a Pub/Sub message.
 */
public class PubSubToArchive extends DoFn<PubsubMessage, Archive> {
    private static final Logger LOG = LoggerFactory.getLogger(PubSubToArchive.class);

    @ProcessElement
    public void processElement(ProcessContext ctx) {
        PubsubMessage msg = ctx.element();
        LOG.debug("Processing Pub/Sub message {}", msg);
        if (msg == null) {
            LOG.warn("Null message received from subscription");
            return;
        }
        Archive archive = new Archive.Builder()
                .processTimestamp(ctx.timestamp())
                .payload(msg.getPayload())
                .putAllAttributes(msg.getAttributeMap())
                .build();
        LOG.debug("Returning instance {}", archive);
        ctx.output(archive);
    }
}

