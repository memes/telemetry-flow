package com.neudesic.demos.telemetry.models;

import org.immutables.gson.Gson;
import org.immutables.value.Value;

import java.io.Serializable;

import javax.annotation.Nullable;

import ch.hsr.geohash.GeoHash;

@Value.Immutable
@Value.Style(strictBuilder = true, visibility = Value.Style.ImplementationVisibility.PACKAGE, overshadowImplementation = true)
@Gson.TypeAdapters
public interface Position extends Serializable {
    @Nullable
    Double longitude();

    @Nullable
    Double latitude();

    @Value.Derived
    @Gson.Ignore
    default String geohash8() {
        if (longitude() == null || latitude() == null) {
            return null;
        }
        return GeoHash.geoHashStringWithCharacterPrecision(latitude(), longitude(), 8);
    }
}
