package com.neudesic.demos.telemetry.models;

import org.immutables.gson.Gson;
import org.immutables.value.Value;
import org.joda.time.Instant;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;
import java.util.Map;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

/**
 * Defines an immutable telemetry class.
 *
 * Note: the visibility of the implementing class is PUBLIC as need access to the mutator methods.
 */
@Value.Immutable
@Value.Style(strictBuilder = true, visibility = Value.Style.ImplementationVisibility.PUBLIC, overshadowImplementation = true)
@Gson.TypeAdapters
public interface Telemetry extends Serializable {
    static Logger LOG = LoggerFactory.getLogger(Telemetry.class);
    @Nonnull
    Instant ts();

    @Nonnull
    String ua();

    @Nullable
    Position position();

    @Nullable
    Acceleration acceleration();

    Map<String, String> attributes();

    static String UNKNOWN = "unknown";

    @Nullable
    @Gson.Ignore
    String browser();

    @Nullable
    @Gson.Ignore
    String os();
}
