package com.neudesic.demos.telemetry.models;

import org.immutables.gson.Gson;
import org.immutables.value.Value;

import java.io.Serializable;

import javax.annotation.Nullable;

@Value.Immutable()
@Value.Style(strictBuilder = true, visibility = Value.Style.ImplementationVisibility.PACKAGE, overshadowImplementation = true)
@Gson.TypeAdapters
public interface Acceleration extends Serializable {
    @Nullable
    Double x();

    @Nullable
    Double y();

    @Nullable
    Double z();

    /**
     * Derived from x, y and z values, this represents an acceleration magnitude.
     */
    @Value.Derived
    @Gson.Ignore
    default double magnitude() {
        double magnitude = 0.0;
        if (x() != null) {
            magnitude += x() * x();
        }
        if (y() != null) {
            magnitude += y() * y();
        }
        if (z() != null) {
            magnitude += z() * z();
        }
        if (magnitude != 0.0) {
            magnitude = Math.sqrt(magnitude);
        }
        return magnitude;
    }
}
