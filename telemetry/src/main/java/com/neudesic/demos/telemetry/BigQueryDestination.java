package com.neudesic.demos.telemetry;

import com.google.api.services.bigquery.model.TableReference;
import com.google.api.services.bigquery.model.TimePartitioning;

import org.apache.beam.sdk.io.gcp.bigquery.TableDestination;
import org.apache.beam.sdk.options.ValueProvider;
import org.apache.beam.sdk.transforms.SerializableFunction;
import org.apache.beam.sdk.values.ValueInSingleWindow;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BigQueryDestination<T>
        implements SerializableFunction<ValueInSingleWindow<T>, TableDestination> {
    private static final Logger LOG = LoggerFactory.getLogger(BigQueryDestination.class);
    private static final DateTimeFormatter FORMATTER = ISODateTimeFormat.basicDate().withZoneUTC();
    private ValueProvider<String> project;
    private ValueProvider<String> dataset;
    private ValueProvider<String> table;
    private ValueProvider<String> description;

    public BigQueryDestination(ValueProvider<String> project, ValueProvider<String> dataset,
            ValueProvider<String> table, ValueProvider<String> description) {
        LOG.debug("Creating new BigQueryDestination for {}, {}, {}", project, dataset, table);
        this.project = project;
        this.dataset = dataset;
        this.table = table;
        this.description = description;
    }

    @Override
    public TableDestination apply(ValueInSingleWindow<T> input) {
        T element = input.getValue();
        if (element == null) {
            LOG.warn("Input value is null, returning null");
            return null;
        }
        TableReference tableReference = new TableReference();
        tableReference.setProjectId(project.get());
        tableReference.setDatasetId(dataset.get());
        tableReference.setTableId(String.format("%s$%s", table,
                input.getWindow().maxTimestamp().toString(FORMATTER)));
        LOG.debug("table reference for input {} is {}", input.getWindow().maxTimestamp(), tableReference);
        return new TableDestination(tableReference, description.get(),
                new TimePartitioning().setType("DAY"));
        }
}
