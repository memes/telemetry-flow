package com.neudesic.demos.telemetry.models;

import org.immutables.value.Value;
import org.joda.time.Instant;

import java.io.Serializable;
import java.util.Map;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

@Value.Immutable
@Value.Style(strictBuilder = true, visibility = Value.Style.ImplementationVisibility.PACKAGE, overshadowImplementation = true)
public interface Archive extends Serializable {
    @Nullable
    String id();

    @Nullable
    Instant publishTimestamp();

    @Nonnull
    Instant processTimestamp();

    @Nullable
    byte[] payload();

    Map<String, String> attributes();

    @Nullable
    String errors();

    // Add a reference to the generated builder
    class Builder extends ImmutableArchive.Builder {
    }
}
