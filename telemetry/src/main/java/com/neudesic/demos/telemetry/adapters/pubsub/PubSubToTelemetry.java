package com.neudesic.demos.telemetry.adapters.pubsub;

import com.blueconic.browscap.BrowsCapField;
import com.blueconic.browscap.Capabilities;
import com.blueconic.browscap.ParseException;
import com.blueconic.browscap.UserAgentParser;
import com.blueconic.browscap.UserAgentService;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.neudesic.demos.telemetry.models.GsonAdaptersAcceleration;
import com.neudesic.demos.telemetry.models.GsonAdaptersPosition;
import com.neudesic.demos.telemetry.models.GsonAdaptersTelemetry;
import com.neudesic.demos.telemetry.models.ImmutableTelemetry;
import com.neudesic.demos.telemetry.models.Telemetry;

import org.apache.beam.sdk.io.gcp.pubsub.PubsubMessage;
import org.apache.beam.sdk.transforms.DoFn;
import org.joda.time.Instant;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.lang.reflect.Type;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;

/**
 * An adapter that can create a Telemetry instance from a Pub/Sub message.
 */
public class PubSubToTelemetry extends DoFn<PubsubMessage, Telemetry> {
    private static final Logger LOG = LoggerFactory.getLogger(PubSubToTelemetry.class);

    private transient Gson gson_;
    private transient UserAgentParser parser_;

    private Gson gson() {
        if (gson_ == null) {
            synchronized (this) {
                if (gson_ == null) {
                    gson_ = new GsonBuilder()
                            .registerTypeAdapterFactory(new GsonAdaptersAcceleration())
                            .registerTypeAdapterFactory(new GsonAdaptersPosition())
                            .registerTypeAdapterFactory(new GsonAdaptersTelemetry())
                            .registerTypeAdapter(Instant.class, new JsonDeserializer<Instant>() {
                                public Instant deserialize(JsonElement jsonElement, Type typeOfT,
                                        JsonDeserializationContext ctx) throws JsonParseException {
                                    return new Instant(jsonElement.getAsJsonPrimitive().getAsLong());
                                }
                            })
                            .create();
                }
            }
        }
        return gson_;
    }

    private UserAgentParser parser() {
        if (parser_ == null) {
            synchronized (this) {
                if (parser_ == null) {
                    try {
                        parser_ = new UserAgentService().loadParser(
                                Arrays.asList(BrowsCapField.BROWSER, BrowsCapField.PLATFORM));
                    } catch (IOException ioe) {
                        LOG.warn("Caught an IOException, will continue. {}", ioe);
                    } catch (ParseException pe) {
                        LOG.warn("Caught a ParseException, will continue. {}", pe);
                    }
                }
            }
        }
        return parser_;
    }

    @ProcessElement
    public void processElement(ProcessContext ctx) {
        PubsubMessage msg = ctx.element();
        LOG.debug("Processing Pub/Sub message {}", msg);
        if (msg == null) {
            LOG.warn("Null message received from subscription");
            return;
        }
        byte[] payload = msg.getPayload();
        if (payload == null) {
            LOG.warn("Null payload received in message");
            return;
        }
        Telemetry telemetry = gson().fromJson(new String(payload, StandardCharsets.US_ASCII), Telemetry.class);
        Capabilities capabilities = parser().parse(telemetry.ua());
        telemetry = ((ImmutableTelemetry) telemetry)
                .withAttributes(msg.getAttributeMap())
                .withBrowser(capabilities.getBrowser())
                .withOs(capabilities.getPlatform());
        LOG.debug("Returning instance {}", telemetry);
        ctx.output(telemetry);
    }
}
