package com.neudesic.demos.telemetry.adapters.bigquery;

import com.google.api.services.bigquery.model.TableFieldSchema;
import com.google.api.services.bigquery.model.TableRow;
import com.google.api.services.bigquery.model.TableSchema;
import com.neudesic.demos.telemetry.adapters.TimestampConversions;
import com.neudesic.demos.telemetry.models.Archive;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * An adapter that can create a TableRow from an Archive.
 */
public class ArchiveToTableRow {
    private static final Logger LOG = LoggerFactory.getLogger(ArchiveToTableRow.class);
    private static final String PROCESS_TS_FIELD = "process_ts";
    private static final String ID_FIELD = "id";
    private static final String PUBLISH_TS_FIELD = "publish_ts";
    private static final String PAYLOAD_FIELD = "payload";
    private static final String ATTRIBUTES_FIELD = "attributes";
    private static final String ERRORS_FIELD = "errors";
    private static final TableSchema SCHEMA =
            new TableSchema().setFields(Arrays.asList(
                    new TableFieldSchema()
                            .setDescription("Timestamp when the message was processed")
                            .setName(PROCESS_TS_FIELD)
                            .setType("TIMESTAMP")
                            .setMode("REQUIRED"),
                    new TableFieldSchema()
                            .setDescription("Pub/Sub message id, may be null")
                            .setName(ID_FIELD)
                            .setType("STRING")
                            .setMode("NULLABLE"),
                    new TableFieldSchema()
                            .setDescription("Publish timestamp from message, may be null")
                            .setName(PUBLISH_TS_FIELD)
                            .setType("TIMESTAMP")
                            .setMode("NULLABLE"),
                    new TableFieldSchema()
                            .setDescription("Payload data, as raw bytes")
                            .setName(PAYLOAD_FIELD)
                            .setType("BYTES")
                            .setMode("NULLABLE"),
                    new TableFieldSchema()
                            .setDescription("Pub/Sub message attributes, flattened")
                            .setName(ATTRIBUTES_FIELD)
                            .setType("STRING")
                            .setMode("NULLABLE"),
                    new TableFieldSchema()
                            .setDescription("Errors as strings")
                            .setName(ERRORS_FIELD)
                            .setType("STRING")
                            .setMode("NULLABLE")
            ));

    public static TableSchema schema() {
        LOG.debug("returning schema {}", SCHEMA);
        return SCHEMA;
    }

    public static TableRow tableRow(Archive archive) {
        LOG.debug("Converting to table row for {}", archive);
        if (archive == null) {
            LOG.warn("Archive instance is null, returning null");
            return null;
        }
        TableRow row = new TableRow();
        try {
            row.set(PROCESS_TS_FIELD,
                    TimestampConversions.toBigQueryTimestamp(archive.processTimestamp()));
            if (archive.id() != null) {
                row.set(ID_FIELD, archive.id());
            }
            if (archive.publishTimestamp() != null) {
                row.set(PUBLISH_TS_FIELD,
                        TimestampConversions.toBigQueryTimestamp(archive.publishTimestamp()));
            }
            if (archive.payload() != null && archive.payload().length > 0) {
                row.set(PAYLOAD_FIELD, archive.payload());
            }
            Map<String, String> attributes = archive.attributes();
            if (attributes != null && !attributes.isEmpty()) {
                row.set(ATTRIBUTES_FIELD,
                        attributes.entrySet()
                                .stream()
                                .map((entry) -> String.format("%s=%s", entry.getKey(), entry.getValue()))
                        .collect(Collectors.joining(", ")));
            }
        } catch (Throwable t) {
            LOG.warn("Caught an exception, will record in table: {}", t);
            row.set(ERRORS_FIELD, t.toString());
        }
        return row;
    }
}

