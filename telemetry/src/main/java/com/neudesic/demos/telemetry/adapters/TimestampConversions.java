package com.neudesic.demos.telemetry.adapters;

import org.joda.time.Instant;
import org.joda.time.format.ISODateTimeFormat;

import javax.annotation.Nonnull;

public class TimestampConversions {

    private TimestampConversions() {
    }

    public static String toBigQueryTimestamp(@Nonnull Instant ts) {
        return ISODateTimeFormat.dateTime().print(ts);
    }

    public static String toBigQueryDate(@Nonnull Instant ts) {
        return ISODateTimeFormat.date().print(ts);
    }
}
